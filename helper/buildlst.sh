#make list of req to build pkg
NAME=$1
#DEFPATH=$2
#"/home/dan/git/custom_slackbuilds/dspbuilds"
FILEIN=$2
EXPATH=$3
#$DEFPATH'/dspbuilds.lst'
FILEOUT='/tmp/'${1}'l.tmp'
CWD=$(pwd)
#echo $1 $2 $3 $4
#echo $NAME $DEFPATH $FILEIN $EXPATH

WKGPATH=$CWD'/temp/'
grep -xA 9 'NAME: '$NAME $FILEIN | cut -d " " -f2- > $FILEOUT
exec < $FILEOUT
read  NAME  
read  LOCATION
read  FILES
read  VERSION
read  DOWNLOAD
read  DOWNLOAD_x86_64
read  MD5SUM
read  MD5SUM_x86_64
read  REQUIRES
read  SHORT
rm $FILEOUT
REQTMP=$EXPATH$NAME'.bl'
if  [ "$REQUIRES" != "REQUIRES:" ] 
        then
            echo $REQUIRES | awk ' {printf"%i\n",NF
                                    for (i=1; i<=NF; i++) 
                                    printf"%s\n",$i}
                                '>$REQTMP
fi
echo $NAME
if [ -e $REQTMP ]
    then

        exec < $REQTMP
        read NUMDEPS
        for ((i=0 ;i< NUMDEPS;i++))
            do
            	read DEP
				${EXPATH}buildlst.sh $DEP $FILEIN $EXPATH
        done
        rm $REQTMP
fi

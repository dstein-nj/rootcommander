#!/bin/bash
# stages files for a release 
# grenerates slackpkg
CWD=$(pwd)

num=1 #change to read old package
#while IFS= read -r line
#do
    #ver=$line
	ver=$(source rootcomand.ver 2>null ; echo $VERSION)
    major=$(echo $ver | cut -d "." -f1)
    minor=$(echo $ver | cut -d "." -f2)
	point=$(echo $ver | cut -d "." -f3)
    if [ $1 == 'major' ] 
    then
        ((major++))
        minor=0
    	point=0
        echo ^^^ IGNORE THAT ^^
	elif [ $1 == 'minor' ]
	then
		((minor++))
		point=0
		echo ^^^ IGNORE THAT ^^
	else
		((point++))
    fi
#done < "rootcomand.ver"
newver=$major.$minor.$point
echo oldver:$ver newver:$newver
sed -i "/VERSION=/c\VERSION=$newver" rc

rm -r release
mkdir release
mkdir release/var
mkdir release/var/lib
mkdir release/var/lib/rootcomand
mkdir release/var/lib/rootcomand/bl
mkdir release/var/lib/rootcomand/slackbuilds
mkdir release/var/lib/rootcomand/packages

mkdir release/sbin
cp rc release/sbin/
mkdir release/usr
mkdir release/usr/libexec
mkdir release/usr/libexec/rootcomand
cp helper/* release/usr/libexec/rootcomand/
mkdir release/install


cd release/install

#echo          |-----handy-ruler------------------------------------------------------|   >slack-desc
echo rootcomand: rootcomand -Comandline tool for the casual system admin               > slack-desc 
echo rootcomand:                                                                       >> slack-desc
echo rootcomand: A bunch of collection of scripts and commands I have found            >> slack-desc
echo rootcomand: useful maintaining my slackware systems. I am always lookiing for     >> slack-desc
echo rootcomand: sugestions so please feel free to conatact me:                        >> slack-desc
echo rootcomand: dstein614@gmail.com                                                   >> slack-desc
echo rootcomand:                                                                       >> slack-desc
echo rootcomand: git                                                                   >> slack-desc
echo rootcomand:                                                                       >> slack-desc
echo rootcomand:                                                                       >> slack-desc
echo rootcomand:                                                                       >> slack-desc
cd ..
now=$(pwd)
su - root -c "cd $now ; makepkg -c y $CWD/rootcomand-$newver-noarch-${num}_dsp.txz " 
cd $CWD

echo VERSION=$newver > "rootcomand.ver"   

git commit -a -m "Made new Packge for $newver"
su root -c 'chown -R dan:users ./'
